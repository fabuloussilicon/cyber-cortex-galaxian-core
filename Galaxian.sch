<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="O_AUDIO_L" />
        <signal name="O_AUDIO_R" />
        <signal name="O_HSYNC" />
        <signal name="O_VIDEO_R(3:0)" />
        <signal name="O_VIDEO_G(3:0)" />
        <signal name="O_VIDEO_B(3:0)" />
        <signal name="I_BUTTON(8:0)" />
        <signal name="I_RESET" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="clk" />
        <signal name="XLXN_16" />
        <signal name="O_VSYNC" />
        <signal name="XLXN_21" />
        <signal name="XLXN_22(7:0)" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29(15:0)" />
        <signal name="XLXN_30(7:0)" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_34" />
        <port polarity="Output" name="O_AUDIO_L" />
        <port polarity="Output" name="O_AUDIO_R" />
        <port polarity="Output" name="O_HSYNC" />
        <port polarity="Output" name="O_VIDEO_R(3:0)" />
        <port polarity="Output" name="O_VIDEO_G(3:0)" />
        <port polarity="Output" name="O_VIDEO_B(3:0)" />
        <port polarity="Input" name="I_BUTTON(8:0)" />
        <port polarity="Input" name="I_RESET" />
        <port polarity="Input" name="clk" />
        <port polarity="Output" name="O_VSYNC" />
        <blockdef name="mc_top">
            <timestamp>2011-9-6T14:4:41</timestamp>
            <line x2="480" y1="736" y2="736" x1="416" />
            <line x2="480" y1="800" y2="800" x1="416" />
            <line x2="0" y1="288" y2="288" x1="64" />
            <line x2="0" y1="352" y2="352" x1="64" />
            <line x2="0" y1="416" y2="416" x1="64" />
            <line x2="0" y1="480" y2="480" x1="64" />
            <line x2="0" y1="544" y2="544" x1="64" />
            <rect width="64" x="0" y="596" height="24" />
            <line x2="0" y1="608" y2="608" x1="64" />
            <rect width="64" x="0" y="660" height="24" />
            <line x2="0" y1="672" y2="672" x1="64" />
            <line x2="480" y1="288" y2="288" x1="416" />
            <line x2="480" y1="352" y2="352" x1="416" />
            <rect width="64" x="416" y="404" height="24" />
            <line x2="480" y1="416" y2="416" x1="416" />
            <line x2="0" y1="224" y2="224" x1="64" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <rect width="64" x="0" y="148" height="24" />
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="480" y1="96" y2="96" x1="416" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="480" y1="-416" y2="-416" x1="416" />
            <line x2="480" y1="-352" y2="-352" x1="416" />
            <line x2="480" y1="-288" y2="-288" x1="416" />
            <rect width="64" x="416" y="-172" height="24" />
            <line x2="480" y1="-160" y2="-160" x1="416" />
            <rect width="64" x="416" y="-108" height="24" />
            <line x2="480" y1="-96" y2="-96" x1="416" />
            <rect width="64" x="416" y="-44" height="24" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
            <rect width="352" x="64" y="-448" height="1280" />
        </blockdef>
        <blockdef name="Galaxian_Clock">
            <timestamp>2011-9-6T13:6:34</timestamp>
            <rect width="336" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
            <line x2="464" y1="-96" y2="-96" x1="400" />
            <line x2="464" y1="-32" y2="-32" x1="400" />
        </blockdef>
        <blockdef name="T80as">
            <timestamp>2011-9-6T13:20:34</timestamp>
            <rect width="256" x="64" y="-704" height="704" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <line x2="0" y1="-576" y2="-576" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-384" y2="-384" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-192" y2="-192" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="384" y1="-672" y2="-672" x1="320" />
            <line x2="384" y1="-608" y2="-608" x1="320" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="pullup">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-108" y2="-128" x1="64" />
            <line x2="64" y1="-104" y2="-108" x1="80" />
            <line x2="80" y1="-88" y2="-104" x1="48" />
            <line x2="48" y1="-72" y2="-88" x1="80" />
            <line x2="80" y1="-56" y2="-72" x1="48" />
            <line x2="48" y1="-48" y2="-56" x1="64" />
            <line x2="64" y1="-32" y2="-48" x1="64" />
            <line x2="64" y1="-56" y2="-48" x1="48" />
            <line x2="48" y1="-72" y2="-56" x1="80" />
            <line x2="80" y1="-88" y2="-72" x1="48" />
            <line x2="48" y1="-104" y2="-88" x1="80" />
            <line x2="80" y1="-108" y2="-104" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-128" y2="-128" x1="96" />
        </blockdef>
        <block symbolname="mc_top" name="XLXI_1">
            <blockpin signalname="XLXN_14" name="W_CLK_36M" />
            <blockpin signalname="I_RESET" name="I_RESET" />
            <blockpin signalname="XLXN_16" name="W_DCM_LOCKED" />
            <blockpin signalname="XLXN_21" name="W_CPU_MREQn" />
            <blockpin signalname="XLXN_26" name="W_CPU_RDn" />
            <blockpin signalname="XLXN_27" name="W_CPU_WRn" />
            <blockpin signalname="XLXN_28" name="W_CPU_RFSHn" />
            <blockpin signalname="I_BUTTON(8:0)" name="I_PSW(8:0)" />
            <blockpin signalname="XLXN_29(15:0)" name="W_A(15:0)" />
            <blockpin signalname="XLXN_30(7:0)" name="W_BDI(7:0)" />
            <blockpin signalname="XLXN_13" name="CLK_RESET" />
            <blockpin signalname="O_AUDIO_L" name="O_SOUND_OUT_L" />
            <blockpin signalname="O_AUDIO_R" name="O_SOUND_OUT_R" />
            <blockpin signalname="O_HSYNC" name="O_VGA_H_SYNCn" />
            <blockpin signalname="XLXN_31" name="W_CPU_RESETn" />
            <blockpin signalname="XLXN_24" name="W_CPU_NMIn" />
            <blockpin signalname="XLXN_23" name="W_CPU_WAITn" />
            <blockpin signalname="O_VIDEO_R(3:0)" name="O_VGA_R(3:0)" />
            <blockpin signalname="O_VIDEO_G(3:0)" name="O_VGA_G(3:0)" />
            <blockpin signalname="O_VIDEO_B(3:0)" name="O_VGA_B(3:0)" />
            <blockpin signalname="XLXN_22(7:0)" name="W_BDO(7:0)" />
            <blockpin signalname="O_VSYNC" name="O_VGA_V_SYNCn" />
            <blockpin signalname="XLXN_25" name="W_CPU_CLK" />
        </block>
        <block symbolname="Galaxian_Clock" name="XLXI_2">
            <blockpin signalname="XLXN_13" name="RST_IN" />
            <blockpin signalname="clk" name="CLKIN_IN" />
            <blockpin signalname="XLXN_16" name="LOCKED_OUT" />
            <blockpin signalname="XLXN_14" name="CLKFX_OUT" />
            <blockpin name="CLKIN_IBUFG_OUT" />
            <blockpin name="CLK0_OUT" />
        </block>
        <block symbolname="T80as" name="XLXI_3">
            <blockpin signalname="XLXN_31" name="RESET_n" />
            <blockpin signalname="XLXN_25" name="CLK_n" />
            <blockpin signalname="XLXN_23" name="WAIT_n" />
            <blockpin signalname="XLXN_32" name="INT_n" />
            <blockpin signalname="XLXN_24" name="NMI_n" />
            <blockpin signalname="XLXN_34" name="BUSRQ_n" />
            <blockpin signalname="XLXN_22(7:0)" name="DI(7:0)" />
            <blockpin name="M1_n" />
            <blockpin signalname="XLXN_21" name="MREQ_n" />
            <blockpin name="IORQ_n" />
            <blockpin signalname="XLXN_26" name="RD_n" />
            <blockpin signalname="XLXN_27" name="WR_n" />
            <blockpin signalname="XLXN_28" name="RFSH_n" />
            <blockpin name="HALT_n" />
            <blockpin name="BUSAK_n" />
            <blockpin name="DOE" />
            <blockpin signalname="XLXN_29(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_30(7:0)" name="DO(7:0)" />
        </block>
        <block symbolname="pullup" name="XLXI_4">
            <blockpin signalname="XLXN_34" name="O" />
        </block>
        <block symbolname="pullup" name="XLXI_5">
            <blockpin signalname="XLXN_32" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="7040">
        <branch name="O_AUDIO_L">
            <wire x2="2784" y1="2464" y2="2464" x1="2752" />
        </branch>
        <iomarker fontsize="28" x="2784" y="2464" name="O_AUDIO_L" orien="R0" />
        <branch name="O_AUDIO_R">
            <wire x2="2784" y1="2528" y2="2528" x1="2752" />
        </branch>
        <iomarker fontsize="28" x="2784" y="2528" name="O_AUDIO_R" orien="R0" />
        <branch name="O_HSYNC">
            <wire x2="2784" y1="2592" y2="2592" x1="2752" />
        </branch>
        <iomarker fontsize="28" x="2784" y="2592" name="O_HSYNC" orien="R0" />
        <branch name="O_VIDEO_R(3:0)">
            <wire x2="2784" y1="2720" y2="2720" x1="2752" />
        </branch>
        <iomarker fontsize="28" x="2784" y="2720" name="O_VIDEO_R(3:0)" orien="R0" />
        <branch name="O_VIDEO_G(3:0)">
            <wire x2="2784" y1="2784" y2="2784" x1="2752" />
        </branch>
        <iomarker fontsize="28" x="2784" y="2784" name="O_VIDEO_G(3:0)" orien="R0" />
        <branch name="O_VIDEO_B(3:0)">
            <wire x2="2784" y1="2848" y2="2848" x1="2752" />
        </branch>
        <iomarker fontsize="28" x="2784" y="2848" name="O_VIDEO_B(3:0)" orien="R0" />
        <branch name="I_BUTTON(8:0)">
            <wire x2="2272" y1="3040" y2="3040" x1="2176" />
        </branch>
        <iomarker fontsize="28" x="2176" y="3040" name="I_BUTTON(8:0)" orien="R180" />
        <branch name="I_RESET">
            <wire x2="2272" y1="3104" y2="3104" x1="2240" />
        </branch>
        <iomarker fontsize="28" x="2240" y="3104" name="I_RESET" orien="R180" />
        <instance x="1328" y="2912" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_13">
            <wire x2="1280" y1="2352" y2="2688" x1="1280" />
            <wire x2="1328" y1="2688" y2="2688" x1="1280" />
            <wire x2="3152" y1="2352" y2="2352" x1="1280" />
            <wire x2="3152" y1="2352" y2="2976" x1="3152" />
            <wire x2="3152" y1="2976" y2="2976" x1="2752" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="2032" y1="2752" y2="2752" x1="1792" />
            <wire x2="2032" y1="2752" y2="2976" x1="2032" />
            <wire x2="2272" y1="2976" y2="2976" x1="2032" />
        </branch>
        <branch name="clk">
            <wire x2="1328" y1="2880" y2="2880" x1="1296" />
        </branch>
        <iomarker fontsize="28" x="1296" y="2880" name="clk" orien="R180" />
        <branch name="XLXN_16">
            <wire x2="2048" y1="2688" y2="2688" x1="1792" />
            <wire x2="2048" y1="2688" y2="2912" x1="2048" />
            <wire x2="2272" y1="2912" y2="2912" x1="2048" />
        </branch>
        <instance x="2320" y="4784" name="XLXI_3" orien="R0">
        </instance>
        <instance x="2272" y="2880" name="XLXI_1" orien="R0">
        </instance>
        <branch name="O_VSYNC">
            <wire x2="2800" y1="3616" y2="3616" x1="2752" />
        </branch>
        <iomarker fontsize="28" x="2800" y="3616" name="O_VSYNC" orien="R0" />
        <branch name="XLXN_21">
            <wire x2="1680" y1="3232" y2="3776" x1="1680" />
            <wire x2="2768" y1="3776" y2="3776" x1="1680" />
            <wire x2="2768" y1="3776" y2="4176" x1="2768" />
            <wire x2="2272" y1="3232" y2="3232" x1="1680" />
            <wire x2="2768" y1="4176" y2="4176" x1="2704" />
        </branch>
        <branch name="XLXN_22(7:0)">
            <wire x2="2272" y1="4016" y2="4688" x1="2272" />
            <wire x2="2320" y1="4688" y2="4688" x1="2272" />
            <wire x2="2784" y1="4016" y2="4016" x1="2272" />
            <wire x2="2784" y1="3296" y2="3296" x1="2752" />
            <wire x2="2784" y1="3296" y2="4016" x1="2784" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="1936" y1="4304" y2="4864" x1="1936" />
            <wire x2="3056" y1="4864" y2="4864" x1="1936" />
            <wire x2="2320" y1="4304" y2="4304" x1="1936" />
            <wire x2="3056" y1="3232" y2="3232" x1="2752" />
            <wire x2="3056" y1="3232" y2="4864" x1="3056" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="2320" y1="4496" y2="4496" x1="2192" />
            <wire x2="2192" y1="4496" y2="4848" x1="2192" />
            <wire x2="3040" y1="4848" y2="4848" x1="2192" />
            <wire x2="3040" y1="3168" y2="3168" x1="2752" />
            <wire x2="3040" y1="3168" y2="4848" x1="3040" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="2288" y1="4000" y2="4208" x1="2288" />
            <wire x2="2320" y1="4208" y2="4208" x1="2288" />
            <wire x2="2848" y1="4000" y2="4000" x1="2288" />
            <wire x2="2848" y1="3680" y2="3680" x1="2752" />
            <wire x2="2848" y1="3680" y2="4000" x1="2848" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="1888" y1="3296" y2="3760" x1="1888" />
            <wire x2="2752" y1="3760" y2="3760" x1="1888" />
            <wire x2="2752" y1="3760" y2="4304" x1="2752" />
            <wire x2="2272" y1="3296" y2="3296" x1="1888" />
            <wire x2="2752" y1="4304" y2="4304" x1="2704" />
        </branch>
        <branch name="XLXN_27">
            <wire x2="1920" y1="3360" y2="3728" x1="1920" />
            <wire x2="2736" y1="3728" y2="3728" x1="1920" />
            <wire x2="2736" y1="3728" y2="4368" x1="2736" />
            <wire x2="2272" y1="3360" y2="3360" x1="1920" />
            <wire x2="2736" y1="4368" y2="4368" x1="2704" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="1968" y1="3424" y2="3792" x1="1968" />
            <wire x2="2720" y1="3792" y2="3792" x1="1968" />
            <wire x2="2720" y1="3792" y2="4432" x1="2720" />
            <wire x2="2272" y1="3424" y2="3424" x1="1968" />
            <wire x2="2720" y1="4432" y2="4432" x1="2704" />
        </branch>
        <branch name="XLXN_29(15:0)">
            <wire x2="2256" y1="2368" y2="3488" x1="2256" />
            <wire x2="2272" y1="3488" y2="3488" x1="2256" />
            <wire x2="3136" y1="2368" y2="2368" x1="2256" />
            <wire x2="3136" y1="2368" y2="4688" x1="3136" />
            <wire x2="3136" y1="4688" y2="4688" x1="2704" />
        </branch>
        <branch name="XLXN_30(7:0)">
            <wire x2="2272" y1="3552" y2="3552" x1="2208" />
            <wire x2="2208" y1="3552" y2="3744" x1="2208" />
            <wire x2="2864" y1="3744" y2="3744" x1="2208" />
            <wire x2="2864" y1="3744" y2="4752" x1="2864" />
            <wire x2="2864" y1="4752" y2="4752" x1="2704" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="2272" y1="3168" y2="3168" x1="2240" />
            <wire x2="2240" y1="3168" y2="4112" x1="2240" />
            <wire x2="2320" y1="4112" y2="4112" x1="2240" />
        </branch>
        <instance x="2032" y="4400" name="XLXI_5" orien="R0" />
        <branch name="XLXN_32">
            <wire x2="2096" y1="4400" y2="4464" x1="2096" />
            <wire x2="2192" y1="4464" y2="4464" x1="2096" />
            <wire x2="2192" y1="4400" y2="4464" x1="2192" />
            <wire x2="2320" y1="4400" y2="4400" x1="2192" />
        </branch>
        <instance x="1968" y="4624" name="XLXI_4" orien="R0" />
        <branch name="XLXN_34">
            <wire x2="2032" y1="4624" y2="4704" x1="2032" />
            <wire x2="2128" y1="4704" y2="4704" x1="2032" />
            <wire x2="2128" y1="4592" y2="4704" x1="2128" />
            <wire x2="2320" y1="4592" y2="4592" x1="2128" />
        </branch>
    </sheet>
</drawing>