//---------------------------------------------------------------------
// FPGA GALAXIAN   ADDRESS DECDER
//
// Version : 2.01
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
// 2004- 4-30  galaxian modify by K.DEGAWA
// 2004- 5- 6  first release.
// 2004- 8-23  Improvement with T80-IP. 
//---------------------------------------------------------------------
//
//GALAXIAN Address Map
//
// Address      Item(R..read-mode W..wight-mode)        Parts                       
//0000 - 1FFF   CPU-ROM..R                            ( 7H or 7K )    
//2000 - 3FFF   CPU-ROM..R                            ( 7L )
//4000 - 47FF   CPU-RAM..RW                           ( 7N  & 7P ) 
//5000 - 57FF   VID-RAM..RW   
//5800 - 5FFF   OBJ-RAM..RW
//6000 -        SW0..R   LAMP......W
//6800 -        SW1..R   SOUND.....W
//7000 -        DIP..R   
//7001                   NMI_ON....W
//7004                   STARS_ON..W
//7006                   H_FLIP....W
//7007                   V-FLIP....W
//7800          WDR..R   PITCH.....W
//
//W MODE
//6000 - 6002           
//6003                  COIN CNTR                   
//6004 - 6007           SOUND CONTROL(OSC)
//
//6800                  SOUND CONTROL(FS1)
//6801                  SOUND CONTROL(FS2)
//6802                  SOUND CONTROL(FS3)
//6803                  SOUND CONTROL(HIT)
//6805                  SOUND CONTROL(SHOT)
//6806                  SOUND CONTROL(VOL1)
//6807                  SOUND CONTROL(VOL2)
//

module mc_adec(

I_CLK_12M,
I_CLK_6M,
I_CPU_CLK,
I_RSTn,

I_CPU_A,
I_CPU_D,
I_MREQn,
I_RFSHn,
I_RDn,
I_WRn,
I_H_BL,
I_V_BLn,

O_WAITn,
O_NMIn,
O_CPU_ROM_CSn,
O_CPU_RAM_RDn,
O_CPU_RAM_WRn,
O_CPU_RAM_CSn,
O_OBJ_RAM_RDn,
O_OBJ_RAM_WRn,
O_OBJ_RAM_RQn,
O_VID_RAM_RDn,
O_VID_RAM_WRn,
O_SW0_OEn,
O_SW1_OEn,
O_DIP_OEn,
O_WDR_OEn,
O_LAMP_WEn,
O_SOUND_WEn,
O_PITCHn,
O_H_FLIP,
O_V_FLIP,
O_BD_G,
O_STARS_ON

);


input  I_CLK_12M;
input  I_CLK_6M;
input  I_CPU_CLK;
input  I_RSTn;

input  [15:0]I_CPU_A;
input  I_CPU_D;
input  I_MREQn;
input  I_RFSHn;
input  I_RDn;
input  I_WRn;
input  I_H_BL;
input  I_V_BLn;

output O_WAITn;
output O_NMIn;
output O_CPU_ROM_CSn;
output O_CPU_RAM_RDn;
output O_CPU_RAM_WRn;
output O_CPU_RAM_CSn;
output O_OBJ_RAM_RDn;
output O_OBJ_RAM_WRn;
output O_OBJ_RAM_RQn;
output O_VID_RAM_RDn;
output O_VID_RAM_WRn;
output O_SW0_OEn;
output O_SW1_OEn;
output O_DIP_OEn;
output O_WDR_OEn;
output O_LAMP_WEn;
output O_SOUND_WEn;
output O_PITCHn;
output O_H_FLIP;
output O_V_FLIP;
output O_BD_G;
output O_STARS_ON;


wire   [3:0]W_8E1_Q;
wire   [3:0]W_8E2_Q;
wire   [7:0]W_8P_Q,W_8N_Q,W_8M_Q;
reg    [7:0]W_9N_Q;
wire   W_NMI_ONn = W_9N_Q[1]; //  galaxian
//------  CPU WAITn  ---------------------------------------------- 

reg    W_6S1_Q,W_6S1_Qn;
reg    W_6S2_Qn;

assign O_WAITn = W_6S1_Qn;
//assign O_WAITn = 1'b1 ; // No Wait

always@(posedge I_CPU_CLK or negedge I_V_BLn)
begin
   if(I_V_BLn == 1'b0)begin
      W_6S1_Q  <= 1'b0;
      W_6S1_Qn <= 1'b1;
   end
   else begin
      W_6S1_Q  <= ~(I_H_BL | W_8P_Q[2]);
      W_6S1_Qn <=   I_H_BL | W_8P_Q[2];
   end
end

always@(negedge I_CPU_CLK)
begin
   W_6S2_Qn <= ~W_6S1_Q;
end
//------  CPU NMIn  ----------------------------------------------- 
wire  W_V_BL = ~I_V_BLn;
reg   O_NMIn;
always@(posedge W_V_BL or negedge W_NMI_ONn)
begin
   if(~W_NMI_ONn)
      O_NMIn <= 1'b1;
   else
      O_NMIn <= 1'b0;
end
//----------------------------------------------------------------- 
logic_74xx139 U_8E1(

.I_G(I_MREQn),
.I_Sel(I_CPU_A[15:14]),
.O_Q(W_8E1_Q)

);

//--------   CPU_ROM CS    0000 - 3FFF  --------------------------- 
logic_74xx139 U_8E2(

.I_G(I_RDn),
.I_Sel({W_8E1_Q[0],I_CPU_A[13]}),
.O_Q(W_8E2_Q)

);

assign O_CPU_ROM_CSn = W_8E2_Q[0]&W_8E2_Q[1] ;  //   0000 - 3FFF
//-----------------------------------------------------------------
//                  ADDRESS
//    W_8E1_Q[0] = 0000 - 3FFF    ---- CPU_ROM_USE 
//    W_8E1_Q[1] = 4000 - 7FFF    ---- GALAXIAN USE   *1
//    W_8E1_Q[2] = 8000 - BFFF    ---- MOONCREST USE 
//    W_8E1_Q[3] = C000 - FFFF

logic_74xx138 U_8P(

.I_G1(I_RFSHn),
.I_G2a(W_8E1_Q[1]),   // <= *1
.I_G2b(W_8E1_Q[1]),   // <= *1
.I_Sel(I_CPU_A[13:11]),
.O_Q(W_8P_Q)

);

logic_74xx138 U_8N(

.I_G1(1'b1),
.I_G2a(I_RDn),
.I_G2b(W_8E1_Q[1]),   // <= *1
.I_Sel(I_CPU_A[13:11]),
.O_Q(W_8N_Q)

);

logic_74xx138 U_8M(

//.I_G1(W_6S2_Qn),
.I_G1(1'b1), // No Wait
.I_G2a(I_WRn),
.I_G2b(W_8E1_Q[1]),   // <= *1
.I_Sel(I_CPU_A[13:11]),
.O_Q(W_8M_Q)

);

assign O_BD_G        = ~(W_8E1_Q[0]&W_8P_Q[0]);  //
assign O_OBJ_RAM_RQn = W_8P_Q[3];                //

assign O_CPU_RAM_CSn = W_8N_Q[0]&W_8M_Q[0];      //
assign O_CPU_RAM_RDn = W_8N_Q[0];                //
assign O_CPU_RAM_WRn = W_8M_Q[0];                //
assign O_VID_RAM_RDn = W_8N_Q[2];                //
assign O_OBJ_RAM_RDn = W_8N_Q[3];                //
assign O_SW0_OEn     = W_8N_Q[4];                // 
assign O_SW1_OEn     = W_8N_Q[5];                // 
assign O_DIP_OEn     = W_8N_Q[6];                // 
assign O_WDR_OEn     = W_8N_Q[7];                // 

assign O_VID_RAM_WRn = W_8M_Q[2];                // 
assign O_OBJ_RAM_WRn = W_8M_Q[3];                // 
assign O_LAMP_WEn    = W_8M_Q[4];                // 
assign O_SOUND_WEn   = W_8M_Q[5];                // 

assign O_PITCHn      = W_8M_Q[7];                // 

//---  Parts 9N ---------

always@(posedge I_CLK_12M or negedge I_RSTn)
begin
   if(I_RSTn == 1'b0)begin
      W_9N_Q <= 0;
   end 
   else begin
      if(W_8M_Q[6] == 1'b0)begin
         case(I_CPU_A[2:0])
            3'h0 : W_9N_Q[0] <= I_CPU_D;
            3'h1 : W_9N_Q[1] <= I_CPU_D;
            3'h2 : W_9N_Q[2] <= I_CPU_D;
            3'h3 : W_9N_Q[3] <= I_CPU_D;
            3'h4 : W_9N_Q[4] <= I_CPU_D;
            3'h5 : W_9N_Q[5] <= I_CPU_D;
            3'h6 : W_9N_Q[6] <= I_CPU_D;
            3'h7 : W_9N_Q[7] <= I_CPU_D;
         endcase
      end
   end
end

assign O_STARS_ON    = W_9N_Q[4];                // 
assign O_H_FLIP      = W_9N_Q[6];                // 
assign O_V_FLIP      = W_9N_Q[7];                // 


endmodule
