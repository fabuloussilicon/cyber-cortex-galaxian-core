//===============================================================================
// FPGA MOONCRESTA & GALAXIAN    
//      FPGA BLOCK RAM I/F (ALTERA-CYCLONE & XILINX SPARTAN2E)
//
// Version : 2.50
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
// mc_col_rom(6L) added by k.Degawa
//
// 2004- 5- 6  first release.
// 2004- 8-23  Improvement with T80-IP.    K.Degawa
// 2004- 9-18  added Xilinx Device         K.Degawa
//================================================================================
`include "src/mc_conf.v"

//  mc_top.v use
module mc_cpu_ram (

I_CLK,
I_ADDR,
I_D,
I_WE,
I_OE,
O_D

);

input  I_CLK;
input  [9:0]I_ADDR;
input  [7:0]I_D;
input  I_WE;
input  I_OE;
output [7:0]O_D;

wire   [7:0]W_D;
assign O_D = I_OE ? W_D : 8'h00 ;

`ifdef DEVICE_CYCLONE
alt_ram_1024_8 CPURAM_ALT(

.clock(I_CLK),
.address(I_ADDR),
.data(I_D),
.wren(I_WE),
.q(W_D)

);
`endif
`ifdef DEVICE_SPARTAN2E 
RAMB4_S4 CPURAM_X1(

.CLK(I_CLK),
.ADDR(I_ADDR[9:0]),
.DI(I_D[7:4]),
.DO(W_D[7:4]),
.EN(1'b1),
.WE(I_WE),
.RST(1'b0)

);

RAMB4_S4 CPURAM_X0(

.CLK(I_CLK),
.ADDR(I_ADDR[9:0]),
.DI(I_D[3:0]),
.DO(W_D[3:0]),
.EN(1'b1),
.WE(I_WE),
.RST(1'b0)

);
`endif

endmodule

//  mc_video.v use
module mc_obj_ram(

I_CLKA,
I_ADDRA,
I_WEA,
I_CEA,
I_DA,
O_DA,

I_CLKB,
I_ADDRB,
I_WEB,
I_CEB,
I_DB,
O_DB

);

input  I_CLKA,I_CLKB;
input  [7:0]I_ADDRA,I_ADDRB;
input  I_WEA,I_WEB;
input  I_CEA,I_CEB;
input  [7:0]I_DA,I_DB;
output [7:0]O_DA,O_DB;

`ifdef DEVICE_CYCLONE
alt_ram_256_8_8 OBJRAM(

.clock_a(I_CLKA),
.address_a(I_ADDRA),
.wren_a(I_WEA),
.enable_a(I_CEA),
.data_a(I_DA),
.q_a(O_DA),

.clock_b(I_CLKB),
.address_b(I_ADDRB),
.wren_b(I_WEB),
.enable_b(I_CEB),
.data_b(I_DB),
.q_b(O_DB)

);
`endif
`ifdef DEVICE_SPARTAN2E 
RAMB4_S8_S8 OBJRAM(

.CLKA(I_CLKA),
.ADDRA({1'b0,I_ADDRA[7:0]}),
.DIA(I_DA),
.DOA(O_DA),
.ENA(I_CEA),
.WEA(I_WEA),
.RSTA(1'b0),

.CLKB(I_CLKB),
.ADDRB({1'b0,I_ADDRB[7:0]}),
.DIB(I_DB),
.DOB(O_DB),
.ENB(I_CEB),
.WEB(I_WEB),
.RSTB(1'b0)
);
`endif

endmodule


//  mc_video.v use
module mc_vid_ram (

I_CLKA,
I_ADDRA,
I_DA,
I_WEA,
I_CEA,
O_DA,

I_CLKB,
I_ADDRB,
I_DB,
I_WEB,
I_CEB,
O_DB

);

input  I_CLKA,I_CLKB;
input  [9:0]I_ADDRA,I_ADDRB;
input  [7:0]I_DA,I_DB;
input  I_WEA,I_WEB;
input  I_CEA,I_CEB;
output [7:0]O_DA,O_DB;

`ifdef DEVICE_CYCLONE
alt_ram_1024_8_8 VIDRAM(

.clock_a(I_CLKA),
.address_a(I_ADDRA),
.data_a(I_DA),
.wren_a(I_WEA),
.enable_a(I_CEA),
.q_a(O_DA),

.clock_b(I_CLKB),
.address_b(I_ADDRB),
.data_b(I_DB),
.wren_b(I_WEB),
.enable_b(I_CEB),
.q_b(O_DB)

);
`endif
`ifdef DEVICE_SPARTAN2E 
RAMB4_S4_S4 VIDRAM_X1(

.CLKA(I_CLKA),
.ADDRA(I_ADDRA[9:0]),
.DIA(I_DA[7:4]),
.DOA(O_DA[7:4]),
.ENA(I_CEA),
.WEA(I_WEA),
.RSTA(1'b0),

.CLKB(I_CLKB),
.ADDRB(I_ADDRB[9:0]),
.DIB(I_DB[7:4]),
.DOB(O_DB[7:4]),
.ENB(I_CEB),
.WEB(I_WEB),
.RSTB(1'b0)

);

RAMB4_S4_S4 VIDRAM_X0(

.CLKA(I_CLKA),
.ADDRA(I_ADDRA[9:0]),
.DIA(I_DA[3:0]),
.DOA(O_DA[3:0]),
.ENA(I_CEA),
.WEA(I_WEA),
.RSTA(1'b0),

.CLKB(I_CLKB),
.ADDRB(I_ADDRB[9:0]),
.DIB(I_DB[3:0]),
.DOB(O_DB[3:0]),
.ENB(I_CEB),
.WEB(I_WEB),
.RSTB(1'b0)

);
`endif

endmodule

//  mc_video.v use
module mc_lram(

I_CLK,
I_ADDR,
I_WE,
I_D,
O_Dn

);

input  I_CLK;
input  [7:0]I_ADDR;
input  [4:0]I_D;
input  I_WE;
output [4:0]O_Dn;
wire   [4:0]W_D;

`ifdef DEVICE_CYCLONE
assign O_Dn = ~W_D;

alt_ram_256_5 LRAM(

.inclock(I_CLK),
.outclock(~I_CLK),
.address(I_ADDR),
.data(I_D),
.wren(~I_WE),
.q(W_D)

);
`endif
`ifdef DEVICE_SPARTAN2E
reg    [4:0]O_Dn;
always@(negedge I_CLK) O_Dn <= ~W_D[4:0] ;

RAMB4_S8 LRAM(

.CLK(I_CLK),
.ADDR({1'b0,I_ADDR[7:0]}),
.DI({3'b000,I_D}),
.DO(W_D),
.EN(1'b1),
.WE(I_WE),
.RST(1'b0)

);
`endif

endmodule

//  mc_col_pal.v use
`ifdef DEVICE_CYCLONE
module mc_col_rom(

I_CLK,
I_ADDR,
I_OEn,
O_DO

);

input   I_CLK;
input   [4:0]I_ADDR;
input   I_OEn;
output  [7:0]O_DO;
wire    [7:0]W_DO;

assign  O_DO = I_OEn ? 8'h00 : W_DO ;
alt_rom_6l U_6L(

.clock(I_CLK),
.address(I_ADDR),
.q(W_DO)

);


endmodule
`endif
