//---------------------------------------------------------------------
// FPGA MOONCRESTA   CLOCK GEN 
//
// Version : 1.00
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
//---------------------------------------------------------------------



module mc_clock(

I_CLK_36M,
I_DCM_LOCKED,
O_CLK_18M,
O_CLK_12M,
O_CLK_06M,
O_CLK_06Mn

);

input I_CLK_36M;
input I_DCM_LOCKED;
output O_CLK_18M;
output O_CLK_12M;
output O_CLK_06M;
output O_CLK_06Mn;

// 2/3 clock divider(duty 33%)
//I_CLK   1010101010101010101
//c_ff10  0011110011110011110
//c_ff11  0011000011000011000
//c_ff20  0000110000110000110
//c_ff21  0110000110000110000
//O_12M   0000110110110110110
reg [1:0] state;
reg clk_12m;
initial state = 0;
initial clk_12m = 0;

// 2/3 clock         (duty 66%)
always @(posedge I_CLK_36M)
begin
   if (I_DCM_LOCKED == 1) begin
      case (state)
         2'd0: state <= 2'd1;
         2'd1: state <= 2'd2;
         2'd2: state <= 2'd0;
         2'd3: state <= 2'd0;
      endcase

      if (state == 2'd2)
         clk_12m = 0;
      else
         clk_12m = 1;
   end
   else begin
      state <= 2'd0;
      clk_12m = 0;
   end
end

assign O_CLK_12M = clk_12m;

reg CLK_18M;
always @(posedge I_CLK_36M)
begin
   if (I_DCM_LOCKED == 1)
      CLK_18M <= ~ CLK_18M;
   else
      CLK_18M <= 0;
end
assign O_CLK_18M = CLK_18M;

// 1/3 clock divider (duty 50%)
reg CLK_6M;
reg CLK_6Mn;

always @(posedge O_CLK_12M)
begin
   CLK_6M  <= ~CLK_6M;
   CLK_6Mn <= CLK_6M;
end

assign O_CLK_06M = CLK_6M;
assign O_CLK_06Mn = CLK_6Mn;

endmodule
