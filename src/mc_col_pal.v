//===============================================================================
// FPGA MOONCRESTA COLOR-PALETTE
//
// Version : 2.00
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
// 2004- 9-18 added Xilinx Device.  K.Degawa
//================================================================================
`include "src/mc_conf.v"

module mc_col_pal(

I_CLK_12M,
I_CLK_6M,
I_VID,
I_COL,
I_C_BLnX,

O_C_BLX,
O_STARS_OFFn,
O_R,
O_G,
O_B

);

input  I_CLK_12M;
input  I_CLK_6M;
input  [1:0]I_VID;
input  [2:0]I_COL;
input  I_C_BLnX;

output O_C_BLX;
output O_STARS_OFFn;
output [2:0]O_R;
output [2:0]O_G;
output [1:0]O_B;

//---    Parts 6M    --------------------------------------------------------
wire   [6:0]W_6M_DI = {I_COL[2:0],I_VID[1:0],~(I_VID[0]|I_VID[1]),I_C_BLnX};
reg    [6:0]W_6M_DO;

wire   W_6M_CLR = W_6M_DI[0]|W_6M_DO[0];
assign O_C_BLX = ~(W_6M_DI[0]|W_6M_DO[0]);
assign O_STARS_OFFn = W_6M_DO[1];

always@(posedge I_CLK_6M or negedge W_6M_CLR)
begin
   if(W_6M_CLR==1'b0)
      W_6M_DO <= 7'h00;
   else
      W_6M_DO <= W_6M_DI;
end
//---    COL ROM     --------------------------------------------------------
wire   [4:0]W_COL_ROM_A = W_6M_DO[6:2];
wire   [7:0]W_COL_ROM_DO;
wire   W_COL_ROM_OEn = W_6M_DO[1];

`ifdef DEVICE_CYCLONE
mc_col_rom COL_ROM(

.I_CLK(I_CLK_12M),
.I_ADDR(W_COL_ROM_A),
.O_DO(W_COL_ROM_DO),
.I_OEn(W_COL_ROM_OEn)

);
`endif
`ifdef DEVICE_SPARTAN2E
GALAXIAN_6L COL_ROM(
.CLK(I_CLK_12M),
.ADDR(W_COL_ROM_A),
.DATA(W_COL_ROM_DO),
.ENA(1'b1)
);
//RAMB4_S8 col_rom00(
//
//.CLK(I_CLK_12M),
//.ADDR({4'b0000,W_COL_ROM_A[4:0]}),
//.DI(8'h00),
//.DO(W_COL_ROM_DO),
//.EN(1'b1),
//.WE(1'b0),
//.RST(1'b0)
//
//);
`endif
//---    VID OUT     --------------------------------------------------------
assign O_R[0] = W_COL_ROM_DO[2];
assign O_R[1] = W_COL_ROM_DO[1];
assign O_R[2] = W_COL_ROM_DO[0];

assign O_G[0] = W_COL_ROM_DO[5];
assign O_G[1] = W_COL_ROM_DO[4];
assign O_G[2] = W_COL_ROM_DO[3];

assign O_B[0] = W_COL_ROM_DO[7];
assign O_B[1] = W_COL_ROM_DO[6];


endmodule
