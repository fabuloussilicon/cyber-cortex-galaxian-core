//---------------------------------------------------------------------
// FPGA MOONCRESTA   H & V COUNTER 
//
// Version : 2.00
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
// 2004- 9-22  
//---------------------------------------------------------------------
//  MoonCrest hv_count
//  H_CNT   0 - 255 , 384 - 511  Total 384 count
//  V_CNT   0 - 255 , 504 - 511  Total 264 count
//-----------------------------------------------------------------------------------------
// H_CNT[0],H_CNT[1],H_CNT[2],H_CNT[3],H_CNT[4],H_CNT[5],H_CNT[6],H_CNT[7],H_CNT[8],  
//    1 H     2 H      4H       8H       16 H     32H      64 H     128 H   256 H
//-----------------------------------------------------------------------------------------
// V_CNT[0], V_CNT[1], V_CNT[2], V_CNT[3], V_CNT[4], V_CNT[5], V_CNT[6], V_CNT[7]  
//    1 V      2 V       4 V       8 V       16 V      32 V      64 V     128 V 
//-----------------------------------------------------------------------------------------

module mc_hv_count(

I_CLK,     //  6MHz
I_RSTn,

O_H_CNT,
O_H_SYNC,
O_H_BL,
O_V_CNT,
O_V_SYNC,
O_V_BLn,
O_V_BL2n,
O_C_BLn

);

input  I_CLK,I_RSTn;
output [8:0]O_H_CNT;
output O_H_SYNC;
output O_H_BL;
output O_V_BL2n;
output [7:0]O_V_CNT;
output O_V_SYNC;
output O_V_BLn;

output O_C_BLn;

//-------   H_COUNT   ----------------------------------------   
reg    [8:0]H_CNT;
always@(posedge I_CLK)
begin
   H_CNT <= H_CNT==255 ? 384 : H_CNT +1 ;
end
assign O_H_CNT  = H_CNT[8:0];

//-------   H_SYNC     ----------------------------------------

reg	H_SYNC;
always@(posedge H_CNT[4] or negedge H_CNT[8]) 
begin
   if(H_CNT[8]==1'b0) H_SYNC <= 1'b0;
   else   H_SYNC <= (~H_CNT[6]& H_CNT[5]);
end

assign O_H_SYNC = H_SYNC;
//-------   H_BL     ------------------------------------------

reg    H_BL;

always@(posedge I_CLK)
begin
   case(H_CNT[8:0])
      387:H_BL<=1'b1;
      503:H_BL<=1'b0;
    default:;
   endcase
end

assign O_H_BL = H_BL;
//-------   V_COUNT   ----------------------------------------   
reg    [8:0]V_CNT;
always@(posedge H_SYNC or negedge I_RSTn)
begin
   if(I_RSTn==1'b0)
      V_CNT <= 0;
   else
      V_CNT <= V_CNT==255 ? 504 : V_CNT +1 ;
end
assign O_V_CNT  = V_CNT[7:0];
assign O_V_SYNC = V_CNT[8];

//-------   V_BLn    ------------------------------------------

reg    V_BLn;
always@(posedge H_SYNC)
begin
   case(V_CNT[7:0])
    239: V_BLn <= 0;
     15: V_BLn <= 1;
    default:;
   endcase
end

reg    V_BL2n;
always@(posedge H_SYNC)
begin
   case(V_CNT[7:0])
    239: V_BL2n <= 0;
     16: V_BL2n <= 1;
    default:;
   endcase
end

assign O_V_BLn  = V_BLn;
assign O_V_BL2n = V_BL2n;
//-------   C_BLn     ------------------------------------------

assign O_C_BLn = ~(~V_BLn | H_CNT[8]);

endmodule
