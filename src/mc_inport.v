//---------------------------------------------------------------------
// FPGA MOONCRESTA   INPORT 
//
// Version : 1.01
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
// 2004-4-30  galaxian modify by K.DEGAWA
//---------------------------------------------------------------------

//    DIP SW        0     1     2     3     4     5     
//---------------------------------------------------------------
//  COIN CHUTE 
// 1 COIN/1 PLAY   1'b0  1'b0         
// 2 COIN/1 PLAY   1'b1  1'b0 
// 1 COIN/2 PLAY   1'b0  1'b1         
// FREE PLAY       1'b1  1'b1 
//   BOUNS
//                             1'b0  1'b0         
//                             1'b1  1'b0 
//                             1'b0  1'b1         
//                             1'b1  1'b1
//   LIVES 
//     2                                   1'b0
//     3                                   1'b1

module mc_inport(

I_COIN1,      //  ACTIVE HI
I_COIN2,      //  ACTIVE HI
I_1P_LE,      //  ACTIVE HI
I_1P_RI,      //  ACTIVE HI
I_1P_SH,      //  ACTIVE HI
I_2P_LE,
I_2P_RI,
I_2P_SH,
I_1P_START,   //  ACTIVE HI
I_2P_START,   //  ACTIVE HI

I_SW0_OEn,
I_SW1_OEn,
I_DIP_OEn,

O_D

);

input  I_COIN1;
input  I_COIN2;
input  I_1P_LE;
input  I_1P_RI;
input  I_1P_SH;
input  I_2P_LE;
input  I_2P_RI;
input  I_2P_SH;
input  I_1P_START;
input  I_2P_START;

input  I_SW0_OEn;
input  I_SW1_OEn;
input  I_DIP_OEn;

output [7:0]O_D;

wire   W_TABLE = 0;   //  UP TYPE = 0;

wire   [5:0]W_DIP_D  = {1'b0,1'b1,1'b0,1'b0,1'b0,1'b0};
wire   [7:0]W_SW0_DI = {      1'b0,      1'b0,   W_TABLE,   I_1P_SH,   I_1P_RI,   I_1P_LE,   I_COIN2,   I_COIN1};
wire   [7:0]W_SW1_DI = {W_DIP_D[1],W_DIP_D[0],      1'b0,   I_2P_SH,   I_2P_RI,   I_2P_LE,I_2P_START,I_1P_START};
wire   [7:0]W_DIP_DI = {      1'b0,      1'b0,      1'b0,      1'b0,W_DIP_D[5],W_DIP_D[4],W_DIP_D[3],W_DIP_D[2]};

wire   [7:0]W_SW0_DO = I_SW0_OEn ? 8'h00 : W_SW0_DI;
wire   [7:0]W_SW1_DO = I_SW1_OEn ? 8'h00 : W_SW1_DI;
wire   [7:0]W_DIP_DO = I_DIP_OEn ? 8'h00 : W_DIP_DI;

assign O_D = W_SW0_DO | W_SW1_DO | W_DIP_DO ;

endmodule