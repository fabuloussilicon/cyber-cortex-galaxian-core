//===============================================================================
// FPGA MOONCRESTA VIDEO-LD_PLS_GEN
//
// Version : 2.00
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
// 2004- 9-22 The problem which missile didn't sometimes come out from was improved.
//================================================================================


module mc_ld_pls(

I_CLK_6M,
I_H_CNT,
I_3D_DI,

O_LDn,
O_CNTRLDn,
O_CNTRCLRn,
O_COLLn,
O_VPLn,
O_OBJDATALn,
O_MLDn,
O_SLDn

);

input  I_CLK_6M;
input  [8:0]I_H_CNT;
input  I_3D_DI;

output O_LDn;
output O_CNTRLDn;
output O_CNTRCLRn;
output O_COLLn;
output O_VPLn;
output O_OBJDATALn;
output O_MLDn;
output O_SLDn;

reg    W_5C_Q;
always@(negedge I_CLK_6M)
   W_5C_Q <= I_H_CNT[0];

//    Parts 4D
wire   W_4D1_G = ~(I_H_CNT[0]&I_H_CNT[1]&I_H_CNT[2]);
wire   [3:0]W_4D1_Q;
wire   [3:0]W_4D2_Q;

logic_74xx139 U_4D1(

.I_G(W_4D1_G),
.I_Sel({I_H_CNT[8],I_H_CNT[3]}),
.O_Q(W_4D1_Q)

);

logic_74xx139 U_4D2(

.I_G(W_5C_Q),
.I_Sel(I_H_CNT[2:1]),
.O_Q(W_4D2_Q)

);

//    Parts 4C
wire   [3:0]W_4C1_Q;
wire   [3:0]W_4C2_Q;

logic_74xx139 U_4C1(

.I_G(W_4D2_Q[1]),
.I_Sel({I_H_CNT[8],I_H_CNT[3]}),
.O_Q(W_4C1_Q)

);

reg    W_4C1_Q3;
always@(posedge  I_CLK_6M)    // 2004-9-22 added
   W_4C1_Q3 <= W_4C1_Q[3];

reg    W_4C2_B;
always@(posedge W_4C1_Q3) 
   W_4C2_B <= I_3D_DI;

logic_74xx139 U_4C2(

.I_G(W_4D1_Q[3]),
.I_Sel({W_4C2_B,~(I_H_CNT[6]&I_H_CNT[5]&I_H_CNT[4]&I_H_CNT[3])}),
.O_Q(W_4C2_Q)

);

assign O_LDn       = W_4D1_G;
assign O_CNTRLDn   = W_4D1_Q[2];
assign O_CNTRCLRn  = W_4D1_Q[0];
assign O_COLLn     = W_4D2_Q[2];
assign O_VPLn      = W_4D2_Q[0];
assign O_OBJDATALn = W_4C1_Q[2];
assign O_MLDn      = W_4C2_Q[0];
assign O_SLDn      = W_4C2_Q[1];


endmodule
