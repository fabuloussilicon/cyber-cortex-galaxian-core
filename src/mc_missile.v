//===============================================================================
// FPGA MOONCRESTA VIDEO-MISSILE
//
// Version : 2.00
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
// 2004- 9-22 The problem which missile didn't sometimes come out from was improved.
//================================================================================


module mc_missile(

I_CLK_18M,
I_CLK_6M,
I_C_BLn_X,
I_MLDn,
I_SLDn,
I_HPOS,

O_MISSILEn,
O_SHELLn

);

input  I_CLK_6M,I_CLK_18M;
input  I_C_BLn_X;
input  I_MLDn;
input  I_SLDn;
input  [7:0]I_HPOS;

output O_MISSILEn;
output O_SHELLn;

reg    [7:0]W_45R_Q;

always@(posedge I_CLK_6M)
begin
   if(I_MLDn==1'b0)
      W_45R_Q <= I_HPOS;
   else begin
      if(I_C_BLn_X)
         W_45R_Q <= W_45R_Q +1;
      else
         W_45R_Q <= W_45R_Q ;
   end
end

reg    W_5P1_Q;
reg    W_5P1_CLK; 

always@(posedge I_CLK_18M)
   W_5P1_CLK <= ~((&W_45R_Q[7:2])&W_5P1_Q);

always@(posedge W_5P1_CLK or negedge I_MLDn)
begin
   if(I_MLDn==1'b0)
      W_5P1_Q <= 1'b1;
   else
      W_5P1_Q <= 1'b0;
end

assign O_MISSILEn = W_5P1_CLK;

reg    [7:0]W_45S_Q;
always@(posedge I_CLK_6M)
begin
   if(I_SLDn==1'b0)
      W_45S_Q <= I_HPOS;
   else begin
      if(I_C_BLn_X)
         W_45S_Q <= W_45S_Q +1;
      else
         W_45S_Q <= W_45S_Q ;
   end
end

reg    W_5P2_Q;
reg    W_5P2_CLK;

always@(posedge I_CLK_18M)
   W_5P2_CLK <= ~((&W_45S_Q[7:2])&W_5P2_Q);

always@(posedge W_5P2_CLK or negedge I_SLDn)
begin
   if(I_SLDn==1'b0)
      W_5P2_Q <= 1'b1;
   else
      W_5P2_Q <= 1'b0;
end

assign O_SHELLn = W_5P2_CLK;


endmodule