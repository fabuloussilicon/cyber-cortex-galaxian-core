//===============================================================================
// FPGA MOONCRESTA SOUND I/F
//
// Version : 1.00
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
//================================================================================


module mc_sound_a(

I_CLK_12M,
I_CLK_6M,
I_H_CNT1,
I_BD,
I_PITCHn,
I_VOL1,
I_VOL2,

O_SDAT,
O_DO

);

input  I_CLK_12M;
input  I_CLK_6M;
input  I_H_CNT1;
input  [7:0]I_BD;
input  I_PITCHn;
input  I_VOL1;
input  I_VOL2;

output [3:0]O_DO;
output [7:0]O_SDAT;

reg    W_PITCHn;
reg    W_89K_LDn;
reg    [7:0]W_89K_Q;
reg    [7:0]W_89K_LDATA;
reg    [3:0]W_6T_Q;

always@(posedge I_CLK_12M) 
begin
   W_PITCHn  <= I_PITCHn;
   W_89K_LDn <= ~(&W_89K_Q[7:0]);
end

// Parts 9J
always@(posedge W_PITCHn) W_89K_LDATA <= I_BD;

always@(posedge I_H_CNT1)
begin
   if(~W_89K_LDn)
      W_89K_Q <= W_89K_LDATA;
   else
      W_89K_Q <= W_89K_Q + 1;  
end

always@(negedge W_89K_LDn) W_6T_Q <= W_6T_Q + 1;
assign O_DO = W_6T_Q;

reg    [7:0]W_SDAT0;
reg    [7:0]W_SDAT2;
reg    [7:0]W_SDAT3;
always@(posedge I_CLK_6M)
begin
   W_SDAT0 <= W_6T_Q[0]==1'b0 ? 8'd0 : 8'd42 ;
   W_SDAT2 <= W_6T_Q[2]==1'b0 ? 8'd0 : I_VOL1 ? 8'd105 : 8'd57 ;
   W_SDAT3 <= W_6T_Q[3]==1'b0 ? 8'd0 : I_VOL2 ? 8'd72  : 8'd0  ;
end

assign O_SDAT = W_SDAT0 + W_SDAT2 + W_SDAT3 + 8'd20 ;


endmodule