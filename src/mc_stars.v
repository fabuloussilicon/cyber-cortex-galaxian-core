//===============================================================================
// FPGA MOONCRESTA STARS
//
// Version : 2.00
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
// 2004- 9-22 
//================================================================================


module mc_stars(

I_CLK_18M,
I_CLK_6M,
I_H_FLIP,
I_V_SYNC,
I_8HF,
I_256HnX,
I_1VF,
I_2V,
I_STARS_ON,
I_STARS_OFFn,

O_R,
O_G,
O_B,
O_NOISE

);

input  I_CLK_18M;
input  I_CLK_6M;
input  I_H_FLIP;
input  I_V_SYNC;
input  I_8HF;
input  I_256HnX;
input  I_1VF;
input  I_2V;
input  I_STARS_ON;
input  I_STARS_OFFn;

output [2:0]O_R;
output [2:0]O_G;
output [1:0]O_B;
output O_NOISE;

wire    W_V_SYNCn = ~I_V_SYNC;

wire    CLK_1C = ~(I_CLK_18M & I_CLK_6M & W_V_SYNCn & I_256HnX);

reg     W_1C_Q1,W_1C_Q2;
always@(posedge CLK_1C or negedge W_V_SYNCn)
begin
   if(W_V_SYNCn==1'b0)begin
      W_1C_Q1 <= 1'b0;
      W_1C_Q2 <= 1'b0;
   end
   else begin
      W_1C_Q1 <= 1'b1;
      W_1C_Q2 <= W_1C_Q1;
   end
end

wire    CLK_1AB = ~(CLK_1C |(~(I_H_FLIP|W_1C_Q2))) ;

reg     [15:0]W_1AB_Q;
reg     W_2D_Qn;
wire    W_3B = W_2D_Qn^W_1AB_Q[4];

always@(posedge CLK_1AB or negedge I_STARS_ON)
begin
   if(I_STARS_ON==1'b0)begin
      W_1AB_Q <= 0;
      W_2D_Qn <= 1'b1;
   end
   else begin
      W_1AB_Q <= {W_1AB_Q[14:0],W_3B};
      W_2D_Qn <= ~W_1AB_Q[15];
   end
end

wire    W_2A = ~(& W_1AB_Q[7:0]);
wire    W_4P = ~(( I_8HF ^ I_1VF ) & W_2D_Qn & I_STARS_OFFn);

assign  O_R[2] = 1'b0 ; 
assign  O_R[1] = (W_2A|W_4P) ? 1'b0 : W_1AB_Q[8] ; 
assign  O_R[0] = (W_2A|W_4P) ? 1'b0 : W_1AB_Q[9] ; 

assign  O_G[2] = 1'b0 ; 
assign  O_G[1] = (W_2A|W_4P) ? 1'b0 : W_1AB_Q[10] ; 
assign  O_G[0] = (W_2A|W_4P) ? 1'b0 : W_1AB_Q[11] ; 

assign  O_B[1] = (W_2A|W_4P) ? 1'b0 : W_1AB_Q[12] ; 
assign  O_B[0] = (W_2A|W_4P) ? 1'b0 : W_1AB_Q[13] ; 

reg     noise;
always@(posedge I_2V) noise <= W_2D_Qn ;
assign  O_NOISE = noise ;


endmodule