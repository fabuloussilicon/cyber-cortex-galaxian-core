//===============================================================================
// FPGA MOONCRESTA VIDO-MIX
//
// Version : 1.00
//
// Copyright(c) 2004 Katsumi Degawa , All rights reserved
//
// Important !
//
// This program is freeware for non-commercial use. 
// An author does no guarantee about this program.
// You can use this under your own risk.
//
//================================================================================


module mc_vedio_mix(

I_VID_R,
I_VID_G,
I_VID_B,
I_STR_R,
I_STR_G,
I_STR_B,

I_C_BLnXX,
I_C_BLX,
I_MISSILEn,
I_SHELLn,

O_R,
O_G,
O_B

);

input  [2:0]I_VID_R;
input  [2:0]I_VID_G;
input  [1:0]I_VID_B;
input  [2:0]I_STR_R;
input  [2:0]I_STR_G;
input  [1:0]I_STR_B;

input  I_C_BLnXX;
input  I_C_BLX;
input  I_MISSILEn;
input  I_SHELLn;

output [2:0]O_R;
output [2:0]O_G;
output [1:0]O_B;

// MISSILE => Yellow ;
// SHELL   => White  ;
wire   W_MS_D = ~(I_MISSILEn & I_SHELLn);
wire   W_MS_R = ~I_C_BLX  & W_MS_D;
wire   W_MS_G = ~I_C_BLX  & W_MS_D;
wire   W_MS_B = ~I_C_BLX  & W_MS_D & ~I_SHELLn ;

assign O_R = I_C_BLnXX ? I_VID_R | I_STR_R | {1'b0,W_MS_R,W_MS_R}: 3'b000 ;
assign O_G = I_C_BLnXX ? I_VID_G | I_STR_G | {1'b0,W_MS_G,W_MS_G}: 3'b000 ;
assign O_B = I_C_BLnXX ? I_VID_B | I_STR_B | {     W_MS_B,W_MS_B}: 2'b00  ;

endmodule