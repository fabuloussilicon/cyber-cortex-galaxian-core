module galaxian_roms(
I_ROM_CLK,
I_ADDR,
O_DATA
);

input I_ROM_CLK;
input [18:0]I_ADDR;
output [7:0]O_DATA;

//CPU-Roms
wire [7:0]U_ROM_D;

GALAXIAN_U U_ROM(
.CLK(I_ROM_CLK),
.ADDR(I_ADDR[10:0]),
.DATA(U_ROM_D),
.ENA(1'b1)
);

wire [7:0]V_ROM_D;

GALAXIAN_V V_ROM(
.CLK(I_ROM_CLK),
.ADDR(I_ADDR[10:0]),
.DATA(V_ROM_D),
.ENA(1'b1)
);

wire [7:0]W_ROM_D;

GALAXIAN_W W_ROM(
.CLK(I_ROM_CLK),
.ADDR(I_ADDR[10:0]),
.DATA(W_ROM_D),
.ENA(1'b1)
);

wire [7:0]Y_ROM_D;

GALAXIAN_Y Y_ROM(
.CLK(I_ROM_CLK),
.ADDR(I_ADDR[10:0]),
.DATA(Y_ROM_D),
.ENA(1'b1)
);

//7L CPU-Rom
wire [7:0]L_ROM_D;

GALAXIAN_7L L_ROM(
.CLK(I_ROM_CLK),
.ADDR(I_ADDR[10:0]),
.DATA(L_ROM_D),
.ENA(1'b1)
);

reg [7:0]DATA_OUT;

//    address map
//--------------------------------------------------
// 0x00000 - 0x007FF       galmidw.u        CPU-ROM
// 0x00800 - 0x00FFF       galmidw.v        CPU-ROM
// 0x01000 - 0x017FF       galmidw.w        CPU-ROM
// 0x01800 - 0x01FFF       galmidw.y        CPU-ROM
// 0x02000 - 0x027FF       7l               CPU-ROM
// 0x04000 - 0x047FF       1k.bin           VID-ROM
// 0x05000 - 0x057FF       1h.bin           VID-ROM
// 0x10000 - 0x3FFFF       mc_wav_2.bin     Sound(Wav)Data
always@(posedge I_ROM_CLK)
begin
	if (I_ADDR <= 18'h7ff) begin
		//u
		DATA_OUT <= U_ROM_D;
	end
	else if (I_ADDR >= 18'h800 && I_ADDR <= 18'hfff) begin
		//v
		DATA_OUT <= V_ROM_D;
	end
	else if (I_ADDR >= 18'h1000 && I_ADDR <= 18'h17ff) begin
		//w
		DATA_OUT <= W_ROM_D;
	end
	else if (I_ADDR >= 18'h1800 && I_ADDR <= 18'h1fff) begin
		//y
		DATA_OUT <= Y_ROM_D;
	end
	else if (I_ADDR >= 18'h2000 && I_ADDR <= 18'h27ff) begin
		//7l
		DATA_OUT <= L_ROM_D;
	end
	else begin
		DATA_OUT <= DATA_OUT;
	end
end

assign O_DATA = DATA_OUT;

endmodule
